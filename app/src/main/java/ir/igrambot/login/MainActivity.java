package ir.igrambot.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ir.igrambot.login.Helpers.Validator;

public class MainActivity extends AppCompatActivity {

    Button btnSend;
    EditText username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.bindView();
        this.setListeners();

    }

    private void bindView(){
        btnSend = findViewById(R.id.btnSend);
        username = findViewById(R.id.userName);
        password = findViewById(R.id.password);
    }

    private void setListeners(){
        btnSend.setOnClickListener(v -> {
            this.login();
        });
    }

    private void login() {
        String usernameInput = username.getText().toString();
        String passwordInput = password.getText().toString();

        // validation
        if(! Validator.validateLogin(usernameInput, passwordInput) ){
            showToast(getString(R.string.validation_failed));
            return;
        }
        // login request
        if(!loginRequest(usernameInput, passwordInput)){
            showToast(getString(R.string.login_failed));
            return;
        }
        startActivity(new Intent(this, WelcomeActivity.class));
        finish();
    }

    private void showToast(String text){
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    //login request
    private Boolean loginRequest(String username, String password){
        String[] validUsername = {"mohammad", "mohammad@gmail.com", "09121077835", "+989121077835"};
        boolean isLogin = false;
        for (String item: validUsername) {
            if(item.equals(username) && password.equals("12345678")){
                isLogin = true;
                break;
            }
        }
        return isLogin;
    }




}