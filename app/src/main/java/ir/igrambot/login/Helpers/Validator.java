package ir.igrambot.login.Helpers;

public class Validator {

    public static boolean validateLogin(String username, String password){
        return validateUsername(username) && validatePassword(password);
    }

    public static boolean validateUsername(String username){
        if(username == null) return false;
        // validate phone
        if (isNumeric(username)){
            return validatePhoneNumber(username);
        }
        // validate email
        if(validateEmail(username)){
            return true;
        }
        // username
        if(validateUser(username)){
            return true;
        }

        return false;
    }


    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }


    public static boolean validatePhoneNumber(String phoneNumber){
        return phoneNumber.matches("(\\+98|0)?9\\d{9}");
    }


    public static boolean validateEmail(String email){
        return email.matches("^[A-Za-z0-9+_.-]+@(.+)$");
    }


    private static boolean validateUser(String username){
        return username.matches("^[a-zA-Z0-9_-]{4,50} *$");
    }


    public static boolean validatePassword(String password){
        return password.length() >= 8;
    }



}
